# jQuery Slide #

Display or hide the matched DOM elements with a sliding motion.

### Get started ###

jQuery Slide is a jQuery plugin, it need jQuery to work.

```shell
$ npm install --save slide-anim
```

### Usage ###

```javascript
$('.myElement').slideUpShow(600);

$('.myElement').slideDownHide('fast', function(){
    // Do something
});
```

See doc at https://www.matthieu-rambert.fr/jqueryslide