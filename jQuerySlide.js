(function($){
    $.fn.extend({
        slideUpShow: function(speed, callback){
            let height = this.css('height');
            this.css('height', 0).show(0, function(){
                $(this).animate({
                    height: height
                }, speed, callback);
            });
            return this;
        },
        slideDownHide: function(speed, callback){
            let height = this.css('height');
            $(this).animate({
                height: 0
            }, speed, function(){
                $(this).hide(0, function(){
                    $(this).css('height', height);
                    callback;
                });
            });
            return this;
        },
        slideLeftShow: function(speed, callback){
            let width = this.css('width');
            $(this).css('width', 0).show(0, function(){
                $(this).animate({
                    width: width
                }, speed, callback);
            });
            return this;
        },
        slideLeftHide: function(speed, callback){

        },
        slideRightShow: function(speed, callback){

        },
        slideRightHide: function(speed, callback){
            let width = this.css('width');
            $(this).animate({
                width: 0
            }, speed, function(){
                $(this).hide(0, function(){
                    $(this).css('width', width);
                    callback;
                });
            });
            return this;
        }
    });
}(jQuery));